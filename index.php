<?php

// CONTROLLER : Gestion de session
session_start();

if(empty($_SESSION))
{
    $_SESSION['connected'] = false;
    $_SESSION['user'] = false;
    $_SESSION['roles'] = [];
    $_SESSION['droits'] = [];
}

// simulation de connexion
// Utilisation du système de connexion
$_SESSION['connected'] = true;

$_SESSION['user'] = (object)[
    "login" => "admin" . "",
    "password" => md5("1234" . "admin"),
];

/*
$_SESSION['user'] = (object)[
    "login" => "user" . "",
    "password" => md5("5678" . "user"),
];
*/


if ($_SESSION['connected'])
{
    // connexion à la base de données
    $dsn = "mysql:host=127.0.0.1;dbname=gest-droits;charset=utf8";
    $user = "gest-droits";
    $password = "UmoITk5cTiQXijm7";
    
    $dbh = new PDO($dsn, $user, $password);
    
    // vérification du compte utilisateur
    
    // CONTROLLER (UserController.php)

    // Récupération des login et password de la session
    $login = $_SESSION['user']->login;
    $password = $_SESSION['user']->password;

    // DAO (UserDao.php)
    $query = "SELECT * 
              FROM `utilisateur` 
              WHERE `login` = :login 
              AND `password` = :password;";

    $sth = $dbh->prepare($query);
    $sth->bindParam(":login", $login);
    $sth->bindParam(":password", $password);
    $sth->execute();

    if ($sth->rowCount())
    {
        // le compte existe
        // retour de la Dao
        $user = (object)$sth->fetch();
    }
    else
    {
        $user = null;
    }
    $sth->closeCursor();

    // Dao : return $user

    // CONTROLLER (UserController)

    if ($user == null)
    {
        // Session est invalide

        // Gestion des Sessions
        session_unset();

        $error_message = "Session invalide." ;

        // VIEW (Template avec message d'erreur)
        echo $error_message;
    }
    else
    {
        // Session est valide
        // Mise à jour de la variable de Session
        $_SESSION['user'] = $user;

        // récupération des rôles

        // MODEL DAO (RoleDao)
        $query = "SELECT `role`.*
                  FROM `role`
                  INNER JOIN `utilisateur_role` 
                    ON `id_role` = `role`.`id`
                  WHERE `id_utilisateur` = :id_utilisateur;
                  ";

        $sth = $dbh->prepare($query);
        $sth->bindParam(":id_utilisateur", $_SESSION['user']->id);
        $sth->execute();
        
        $array = $sth->fetchAll(PDO::FETCH_ASSOC);

        // Conversion du tableau des rôles en objets
        $roles = [];
        foreach($array as $row)
        {
            $roles[] = (object)$row;
        }

        // CONTROLLER (UserController)

        // stockage des rôles
        $_SESSION['roles'] = $roles;

        // MODEL DAO (DroitDao)

        // récupération des droits
        $query = "SELECT DISTINCT `droit`.*
                  FROM `droit`
                  INNER JOIN `role_droit` 
                    ON `role_droit`.`id_droit` = `droit`.`id`
                  INNER JOIN `utilisateur_role` 
                    ON `role_droit`.`id_role` = `utilisateur_role`.`id_role`
                  WHERE `id_utilisateur` = :id_utilisateur;
                  ";

        $sth = $dbh->prepare($query);
        $sth->bindParam(":id_utilisateur", $_SESSION['user']->id);
        $sth->execute();
        
        $array = $sth->fetchAll(PDO::FETCH_ASSOC);

        // Conversion du tableau des droits en objets
        $droits = [];
        foreach($array as $row)
        {
            $droits[] = (object)$row;
        }

        // CONTROLLER (UserController)
        // stockage des droits
        $_SESSION['droits'] = $droits;

        // VIEW (template à mettre à jour)

        // Affichage du message de Bienvenue
        echo "<p>Bienvenue " . $user->login . ".</p>" . "\r\n";

        // Affichage des rôles
        // utilisation de la fonction implode ? (__toString)
        echo "<p>Rôles : ";
        $count = count($roles);
        foreach($roles as $index => $role)
        {
            echo $role->nom;
            if ($index < $count - 1)
            {
                echo ", ";
            }
        }
        echo "</p>" . "\r\n";

        // Affichage des droits
        echo "<p>Droits : ";
        // utilisation de la fonction implode ? (__toString)
        $count = count($droits);
        foreach($droits as $index => $droit)
        {
            echo $droit->nom;
            if ($index < $count - 1)
            {
                echo ", ";
            }
        }
        echo "</p>" . "\r\n";

    }
}
